const express = require('express');
const _       = require("lodash");
const core    = require('./../../core');
const router  = express.Router();

var teamModel = core.importModel("team");

router.post('/', (req, res) => {
    var teams = [];
    teams = teamModel.ParseTeam(req.body, "1", teams);
    teams = teamModel.ParseTeam(req.body, "2", teams);
    teams = teamModel.ParseTeam(req.body, "3", teams);
    teams = teamModel.ParseTeam(req.body, "4", teams);

    //No teams have been selected, return
    if(teams.length == 0){
        res.redirect("/");
        return;
    }
    
    var teamIds = [];
    _.forEach(teams, function(team_stub) {
        teamIds.push(team_stub.id);
    });

    teamModel.GetTeamsFromIds(teamIds).then(function(teamsResult) {
        _.forEach(teamsResult, function(team_result) {
            _.forEach(teams, function(team_stub, key_stub) {
                if(team_result.id == team_stub.id)
                {
                    teams[key_stub]["tier"]        = team_result["tier"];
                    teams[key_stub]["team_name"]   = team_result["team_name"];
                    teams[key_stub]["species"]     = team_result["species"];
                    teams[key_stub]["quantity"]    = team_result["quantity"];
                    teams[key_stub]["keywords"]    = team_result["keywords"];
                    teams[key_stub]["description"] = team_result["description"];
                }
            });
        })

        return teamModel.GetKeywords();
    }).then(function(keywords) {
        var minLevelTeam = _.minBy(teams, 'level');
        _.forEach(teams, function(team, key) {
            teams[key] = teamModel.CalculateMyOdds(team, teams.length, keywords, minLevelTeam.level);
        });

        teams = _.orderBy(teams, ['sum_odds', 'keyword_values']).reverse();

        res.render(core.ImportView("start"), { teams:teams, json_teams:JSON.stringify(teams) });
	});
});

module.exports = router