const express = require('express');
const core    = require('./../../core');
const router  = express.Router();

var team = core.importModel("team");

router.get('/', (req, res) => {
    team.GetTeams().then(function(teams) {
        res.render(core.ImportView("index"), { teams:teams });
	});
});

module.exports = router