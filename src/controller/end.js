const express = require('express');
const _       = require("lodash");
const core    = require('./../../core');
const router  = express.Router();

var team = core.importModel("team");

router.post('/', (req, res) => {
    var teams = JSON.parse(req.body.json_teams);
    var winner_id = req.body.winner_id;

    team.CreateBattle(winner_id, teams).then(function(keywords) {
        res.redirect("/");
    });
});

module.exports = router