const core     = require('./../../core');
const _        = require("lodash");
const bluebird = require('bluebird');

function GetTeams(){
    return core.db.queryAsync('SELECT * FROM team;');
}

function GetTeamsFromIds(ids){
    teams = ids.join();
    return core.db.queryAsync('SELECT * FROM team t WHERE t.id IN ('+teams+');');
}

function GetKeywords(){
    return core.db.queryAsync('SELECT * FROM keyword;');
}

function CreateBattle(winner_id, teams){
    if(winner_id == "null") winner_id = null;
    return core.db.queryAsync('INSERT INTO battle (`winner_team`) VALUES (?)', [winner_id])
    .then(function(result) {
        _.forEach(teams, function(team) {
            core.db.query('INSERT INTO contestant (`battle_id`, `team_id`, `level`, `odds`, `condition`) VALUES (?, ?, ?, ?, ?);',
                [result.insertId, team.id, team.level, team.odds, team.condition])
        });
    });
}

function ParseTeam(body, number, teams){
    var id = body["team_"+number];
    
    if(id != "-1")
    {
        var level     = body["level_"+number];
        if(isNaN(level)) level = 0;
        var odds      = body["odds_"+number];
        if(isNaN(odds)) odds = 0;
        var condition = body["condition_"+number];
        if(isNaN(condition)) condition = 1;

        teams.push({ 
            id:        id, 
            level:     level, 
            odds:      odds, 
            condition: condition 
        });
    }

    return teams;
}

function CalculateMyOdds(team, length, potentialKeywords, minLevel){
    var tier_odds = 0;
    if(team.tier == "C")
        tier_odds = 0.0;
    if(team.tier == "B")
        tier_odds = 1.0;
    if(team.tier == "A")
        tier_odds = 2.0;
    if(team.tier == "S")
        tier_odds = 3.0;

    var quantity_odds = 0;
    if(length == 2){
        quantity_odds = (team.quantity-1) / 4;
    } else if(length == 3){
        quantity_odds = 0.5 - (((team.quantity-1) / 4));
    } else if(length == 4){
        quantity_odds = 1 - (((team.quantity-1) / 4));
    }

    var level_odds = (team.level-minLevel) / 15;

    var condition_odds = ((team.condition-1) / 4) * 2;

    keyword_values = 0;
    var teamKeywords = team.keywords.split(", ");
    _.forEach(teamKeywords, function(teamKeyword) {
        _.forEach(potentialKeywords, function(potentialKeyword) {
            if(teamKeyword == potentialKeyword.name){
                if(potentialKeyword.opinion == "Good"){
                    keyword_values = keyword_values + 1;
                } else {
                    keyword_values = keyword_values - 1;
                }
            }
        });
    });

    team["sum_odds"] = (tier_odds + quantity_odds + level_odds + condition_odds);
    team["tier_odds"] = tier_odds.toFixed(2);
    team["quantity_odds"] = quantity_odds.toFixed(2);
    team["level_odds"] = level_odds.toFixed(2);
    team["condition_odds"] = condition_odds.toFixed(2);
    team["keyword_values"] = keyword_values.toFixed(2);
    return team;
}

module.exports = {
    GetTeams,
    GetTeamsFromIds,
    GetKeywords,
    CreateBattle,
    ParseTeam,
    CalculateMyOdds
}