module.exports = {
    "up": "CREATE TABLE `team` ( `id` int(11) NOT NULL AUTO_INCREMENT, `tier` varchar(45) DEFAULT NULL, `team_name` varchar(45) DEFAULT NULL, `species` varchar(45) DEFAULT NULL, `quantity` varchar(45) DEFAULT NULL, `keywords` varchar(45) DEFAULT NULL, `description` varchar(255) DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;",
    "down": "DROP TABLE `team`;"
}