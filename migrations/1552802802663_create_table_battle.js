module.exports = {
    "up": "CREATE TABLE `battle` (`id` int(11) NOT NULL AUTO_INCREMENT,`winner_team` int(11) DEFAULT NULL,PRIMARY KEY (`id`),KEY `fk_winner_team_idx` (`winner_team`),CONSTRAINT `fk_winner_team` FOREIGN KEY (`winner_team`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;",
    "down": "DROP TABLE `battle`"
}