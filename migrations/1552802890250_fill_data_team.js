module.exports = {
    "up": function (conn, cb) {
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Spiny Speedsters', 'Cactuar', '1', 'Evasive, Mellow, Squishy, Fast', 'Its needle attack usually one shots most things, and he doesn\'t get hit unless you\'re having bad luck, might lose 1v1.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Blaster Bro', 'Coeurl', '1', 'Evasive, Mellow, Squishy, Fast', 'Has a thunderbolt and a whirlwind attack that\'s amazing. If low condition, might lose 1v1.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Lumbering Loggers', 'Treant', '1', '-', 'Area knockback and single target stone throw.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Hearts of Iron', 'Red Giant', '1', 'Area', 'It smashes most things. But it\'s fairly squishy. It rarely shows up though.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Femme Fatales', 'Royalisk', '1', 'Fast', 'Usually wins over Karlabos if equal condition.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Tidal Divers', 'Karlabos', '1', '-', 'Great area abilities and line water ability.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Ravatogh Runners', 'Wyvern', '2', 'Evasive, Area', 'They are the only team capable of flying around. They are unpredictable though. If they shine, they kill most things.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Crime and Punishment', 'Master Tonberry', '1', 'Fast, Mellow, Area', 'Very jumpy with the lightsaber, many area attacks.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Foppish Hoppers', 'Gigantoad', '1', 'Mellow	', 'Long tongue ability that pierces monster groups.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Noblesse Oblige', 'Kingatrice', '1', 'Evasive, Mellow', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('S', 'Flyaway Duplicorns', 'Duplicorn', '1', 'Area, Fast', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Tusk \'til Dawn', 'Flexitusk', '2', 'Evasive, Fast', 'Overall solid, jumps around and pokes others efficiently.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Obelisks', 'Reaperking', '1', '-', 'Stays back in the start and does a burrow attack. Combines high defense with insane damage if condition is right. This guy got me my first 100k!');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Breakaway Bicorns', 'Spiracorn', '2', 'Evasive, Squishy', 'High damage charge attack.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'EXINERIS Occupation', 'Garchimacera', '3', 'Evasive', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Fishermen Eaters', 'Seadevil', '2', 'Mellow', 'Occasionally fires off a devastating piercing water beam from a distance.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Voracious Villains', 'Voretooth', '4', 'Evasive, Squishy', 'Shines in 1v1 and some 1v2.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Empty Heart', 'Iron Giant', '1', 'Area', 'It doesn\'t smash as much as its red brother. And it\'s still squishy. It rarely shows up as well.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Vesper Guardians', 'Mushussu', '3', 'Squishy, Area', 'They are sneaky, in fights with mainly squishies, these guys bring the well needed area damage needed to win with their tailspins.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', 'Saber Tuskforce	', 'Sabertusk', '5', 'Evasive, Squishy', 'Shines in 1v1 and some 1v2.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('A', '666th Heaven', 'Lich', '1', 'Slow', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Faraway Magnanirs', 'Magnanir', '1', 'Fast', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Runaway Mesmenirs', 'Mesmenir', '4', 'Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Crazy Horns', 'Dualhorn', '1', 'Area', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Dandy Roosters', 'Cockatrice', '1', 'Fast', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Gjallarhorn', 'Ashenhorn', '1', 'Area', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Lantern Shields', 'Skarnbulette', '1', '-', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Goldenteeth', 'Yellowtooth', '2', '-', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Horrendous Horns', 'Dualhorn', '2', 'Area', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Fangs of Havoc', 'Havocfang', '4', 'Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Ruler of the Banks', 'Coral Devil', '1', 'Mellow, Squishy', 'Ranged piercing water cannon.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Fish Eaters', 'Sahagin', '3', '-', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Vessel Eaters', 'Alphagin', '3', 'Area', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Grudge Bearers', 'Tonberry', '3', 'Mellow', 'Weak in big fights, can gang up in 1v1 fights.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Leide Antlers', 'Anak Stag', '1', 'Clumsy', 'Only pick if high condition');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Head Hunters', 'Reaper', '1', 'Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'REDRUN!', 'Redlegs', '1', 'Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Longstockings', 'Hundlegs', '3', '-', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'The Bodyguards', 'Yojimbo', '1', 'Mellow, Squishy', 'In 1v1, he has a chance to do an instakill attack. If he doesn\'t he\'s gonna lose.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Killer for Hire', 'Ronin', '1', 'Mellow, Squishy', 'In 1v1, he has a chance to do an instakill attack. If he doesn\'t he\'s gonna lose.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Galviano Gore Club', 'Sparkshears', '1', 'Area, Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Grim Circus', 'Arachne', '1', 'Area, Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Moon Barkers', 'Imp', '3', 'Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'The Stingers', 'Reapertail', '5', 'Squishy', 'Mainly a pick for big fights if odds are high. Yolo.');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('B', 'Stunning Hens', 'Basilisk', '1', '-', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('C', 'Brass Necks', 'Arok', '2', 'Clumsy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('C', 'Elegant Elephant', 'Garulessa', '1', 'Slow', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('C', 'Gray Heels', 'Arba', '2', 'Clumsy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('C', 'Spiky Shields', 'Bulette', '3', '-', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('C', 'Midnight Walkers', 'Goblin', '5', 'Squishy', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('C', 'Alstor Long Noses', 'Garula', '3', 'Clumsy, Slow', '-');");
        conn.query("INSERT INTO `team` (`tier`, `team_name`, `species`, `quantity`, `keywords`, `description`) VALUES ('C', 'Funny Bones', 'Skeleton', '5', 'Squishy', '-');");
        cb();
    },
    "down": function (conn, cb) {cb();}
}