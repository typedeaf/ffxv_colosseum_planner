module.exports = {
    "up": function (conn, cb) {
        conn.query("INSERT INTO `keyword` (`name`, `opinion`) VALUES ('Evasive', 'Good');");
        conn.query("INSERT INTO `keyword` (`name`, `opinion`) VALUES ('Mellow', 'Good');");
        conn.query("INSERT INTO `keyword` (`name`, `opinion`) VALUES ('Fast', 'Good');");
        conn.query("INSERT INTO `keyword` (`name`, `opinion`) VALUES ('Area', 'Good');");
        conn.query("INSERT INTO `keyword` (`name`, `opinion`) VALUES ('Clumsy', 'Bad');");
        conn.query("INSERT INTO `keyword` (`name`, `opinion`) VALUES ('Squishy', 'Bad');");
        conn.query("INSERT INTO `keyword` (`name`, `opinion`) VALUES ('Slow', 'Bad');");
        cb();
    },
    "down": "TRUNCATE `keyword`"
}