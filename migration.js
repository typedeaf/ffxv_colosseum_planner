const core    = require('./core');
var mysql     = require('mysql');
var migration = require('mysql-migrations');

var connection = mysql.createPool({
  connectionLimit : 10,
  host     : core.config.db.host,
  user     : core.config.db.user,
  password : core.config.db.password,
  database : core.config.db.database
});

migration.init(connection, __dirname + '/migrations');