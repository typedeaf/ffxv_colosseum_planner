//External import
const bluebird = require('bluebird');
const mysql    = require('mysql');
const path     = require('path');
const fs       = require('fs');

//Variables
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
var db;

function init() {

    db = mysql.createConnection({
        host     : config.db.host,
        user     : config.db.user,
        password : config.db.password,
        database : config.db.database
      });
    db.connect();
    bluebird.promisifyAll(db);
    
	module.exports.db = db;
}

function importModel(model) {
	return require(path.join(__dirname, "src", "model", model));
}

function importController(controller) {
	return require(path.join(__dirname, "src", "controller", controller));
}

function ImportView(view){
	return view+".html";
}

module.exports = {
	init,
	importModel,
    importController,
    ImportView,
    config
}