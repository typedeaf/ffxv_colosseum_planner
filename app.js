const express    = require('express');
const nunjucks   = require('nunjucks');
const path       = require('path');
const bodyParser = require('body-parser')
const core       = require('./core');
const app        = express();

// Configure
core.init();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(express.static('public'))

nunjucks.configure(path.join(__dirname, "src", "view"), {
  autoescape: true,
  express: app
});

// App
app.use('/',      core.importController("index"));
app.use('/start', core.importController("start"));
app.use('/end',   core.importController("end"));

app.listen(core.config.port, core.config.host);
console.log(`Running on http://${core.config.host}:${core.config.port}`);